Feature: Content
  In order to test some basic Behat functionality
  As a website user
  I need to be able to see that the Drupal and Drush drivers are working

  @api @nodes
  Scenario: Create many nodes
    Given "page" content:
      | title    |
      | Page one |
      | Page two |
    And "article" content:
      | title          |
      | First article  |
      | Second article |
    And I am logged in as a user with the "administrator" role
    When I go to "admin/content"
    Then I should see "Page one"
    And I should see "Page two"
    And I should see "First article"
    And I should see "Second article"

  @api @users
  Scenario: Create users
    Given users:
      | name     | mail            | status |
      | Test Behat User | test_behat_user@example.com | 1      |
    And I am logged in as a user with the "administrator" role
    When I visit "admin/people"
    Then I should see the link "Test Behat User"

  @api @login
  Scenario: Login as a user created during this scenario
    Given users:
      | name            | status | mail                  | roles          |
      | Behat test user | 1      | behattest@example.com | administrator |
    When I am logged in as "Behat test user"
    And I am at "/admin"
    Then I should see "Content"
    And I should see "People"
    And I should see "Extend"

  @api @terms
  Scenario: Create many terms
    Given "tags" terms:
      | name     |
      | Aardvark |
      | Antelope |
    And I am logged in as a user with the "administrator" role
    When I go to "admin/structure/taxonomy/manage/tags/overview"
    Then I should see "Aardvark"
    And I should see "Antelope"

  @api @authorship
  Scenario: Create nodes with specific authorship
    Given users:
      | name     | mail            | status |
      | Test Behat User | test_behat@example.com | 1      |
    And "article" content:
      | title          | author   | promote |
      | Article by Test Behat User | Test Behat User | 1       |
    When I am logged in as a user with the "administrator" role
    And I am on "/admin/content"
    Then I should see the link "Article by Test Behat User"

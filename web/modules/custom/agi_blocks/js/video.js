(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.video = {
    attach: function (context, settings) {
      $('.modal').on('hidden.bs.modal', function () {
        $('.modal').find('video').trigger('pause');
      });
    }
  };
})(jQuery, Drupal);

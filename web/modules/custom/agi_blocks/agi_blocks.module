<?php

use Drupal\node\NodeInterface;

/**
 * Implements hook_theme().
 */
function agi_blocks_theme($existing, $type, $theme, $path) {
  return [
    // Name of the theme hook. This is used in the controller to trigger the hook.
    'agi_reviews_blocks' => [
      'template' => 'agi-reviews-block',
      'variables' => [
        'block_header' => '',
        'average_review' => 0,
        'total_reviewers' => 0,
        'block_rating_text' => '',
        'block_rating_summary_text' => '',
        'latest_review' => '',
        'latest_review_date' => '',
        'latest_reviewer' => '',
        'category_id' => '',
        'category_label' => '',
      ],
    ],
    'agi_all_reviews_blocks' => [
      'template' => 'agi-all-reviews-block',
      'variables' => [
        'average_review' => 0,
        'number_of_reviews' => 0,
        'block_rating_text' => '',
      ],
    ],
    'agi_online_video_block' => [
      'template' => 'agi-online-video-block',
      'variables' => [
        'bottom_text' => '',
      ],
    ],
    'agi_category_additional_block' => [
      'template' => 'agi-category-additional-block',
      'variables' => [
        'block_header' => '',
        'programs' => [],
      ],
    ],
    'agi_location_address_block' => [
      'template' => 'agi-location-address-block',
      'variables' => [
        'address' => [],
        'location' => '',
      ],
    ],
    'agi_find_class_block' => [
      'template' => 'agi-find-class-block',
      'variables' => [
        'training_classes' => [],
        'block_header' => '',
        'block_text' => '',
      ],
    ],
    'agi_books_links_block' => [
      'template' => 'agi-books-links-block',
      'variables' => [
        'view_content_text' => '',
        'view_errata_text' => '',
        'view_errata_link' => '',
        'register_link' => '',
        'resources_link' => '',
        'buy_amazon_text' => '',
        'buy_amazon_link' => '',
        'buy_barnes_text' => '',
        'buy_barnes_link' => '',
      ],
    ],
    'agi_certificate_program_courses_block' => [
      'template' => 'agi-certificate-program-courses-block',
      'variables' => [
        'courses' => [],
        'block_header' => '',
      ],
    ],
    'agi_certificate_program_location_block' => [
      'template' => 'agi-certificate-program-location-block',
      'variables' => [
        'location_info' => [],
      ],
    ],
  ];
}

/**
 * Implements hook_preprocess_block().
 */
function agi_blocks_preprocess_block(&$variables) {
  $plugin_id = $variables['elements']['#plugin_id'];

  if (isset($variables['content']['#block_content']) && $variables['content']['#block_content']->bundle() === 'adobe_contextual_content') {
    // Getting the current node.
    $current_node = \Drupal::routeMatch()->getParameter('node');

    if ($current_node instanceof NodeInterface) {
      // Checking if the node has the certification configuation.
      $node_has_adobe_certification = $current_node->field_adobe_cert_cat_page->value;

      // Checking if the block has the certification configuation.
      $block_has_adobe_certification = $variables['content']['#block_content']->get('field_has_adobe_certification')->value;

      if ((int) $node_has_adobe_certification !== (int) $block_has_adobe_certification) {
        $variables['content']['body'][0]['#text'] = '';
      }
    }
  }
}

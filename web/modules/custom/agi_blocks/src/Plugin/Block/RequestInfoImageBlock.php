<?php

namespace Drupal\agi_blocks\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\agi_blocks\AgiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the image above the info form.
 *
 * @Block(
 *   id = "agi_block_request_info_image_block",
 *   admin_label = @Translation("AGI Request Info Image Block"),
 * )
 */
class RequestInfoImageBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * Variable that will store the service.
   *
   * @var \Drupal\agi_blocks\AgiService
   */
  protected $agiService;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account, AgiService $agiService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->account = $account;
    $this->agiService = $agiService;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('agi.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Loading the service into a variable.
    $agi_service = $this->agiService;

    // Getting the current node.
    $current_node = \Drupal::routeMatch()->getParameter('node');

    // Markup text for the LB.
    $markup_text = t('Placeholder for the "AGI Request Info Image Block."');

    if ($current_node) {
      // Loading the config.
      $config = $this->getConfiguration();

      $request_info_image_uri = $agi_service->getRequestInfoImageUri($current_node);

      return [
        '#theme' => 'image_style',
        '#style_name' => 'large',
        '#uri' => $request_info_image_uri,
        '#alt' => ($current_node) ? $current_node->label() : t('AGI'),
      ];
    }
    else {
      return [
        '#theme' => 'image_style',
        '#style_name' => 'large',
        '#uri' => 'public://training-courses.jpg',
        '#alt' => ($current_node) ? $current_node->label() : t('AGI'),
      ];
    }

    // Default for the Layout Builder.
    return [
      '#markup' => $markup_text,
    ];

  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
  }
}

<?php

namespace Drupal\agi_blocks\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\agi_blocks\AgiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block with the contextual links for Books.
 *
 * @Block(
 *   id = "agi_block_books_links_block",
 *   admin_label = @Translation("AGI Books Links Block"),
 * )
 */
class BooksLinksBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * Variable that will store the service.
   *
   * @var \Drupal\agi_blocks\AgiService
   */
  protected $agiService;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account, AgiService $agiService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->account = $account;
    $this->agiService = $agiService;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('agi.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Loading the service into a variable.
    $agi_service = $this->agiService;

    // Getting the current node.
    $current_node = \Drupal::routeMatch()->getParameter('node');

    if ($current_node) {
      // Loading the config.
      $config = $this->getConfiguration();

      // Loading the current path.
      $current_path = \Drupal::service('path.current')->getPath();
      $current_path_alias = \Drupal::service('path_alias.manager')
        ->getAliasByPath($current_path);

      // Get the links.
      $amazon_link_field = $current_node->get('field_dcb_amazon_link') ?? '';
      $amazon_link = !$amazon_link_field->isEmpty() ? $amazon_link_field->first()
        ->getValue() : NULL;
      if (!$current_node->get('field_all_lesson_files_external_')->isEmpty()) {
        $resources_link = $current_path_alias . '/resources/' . $current_node->id();
      }
      else {
        $resources_link = '';
      }

    return [
      '#theme' => 'agi_books_links_block',
      '#view_content_text' => $config['view_content_text'] ?? '',
      '#view_errata_text' => $config['view_errata_text'] ?? '',
      '#view_errata_link' => $current_path_alias . '/errata/' . $current_node->id(),
      '#register_link' => $current_path_alias . '/registration/' . $current_node->id(),
      '#resources_link' => $resources_link,
      '#buy_amazon_text' => $config['buy_amazon_text'] ?? '',
      '#buy_amazon_link' => $amazon_link['uri'] ?? '',
    ];
  }
else {

return ['#markup' => t('Placeholder for Books Links Block.'),];

}

}

/**
 * {@inheritdoc}
 */
protected
function blockAccess(AccountInterface $account) {
  return AccessResult::allowedIfHasPermission($account, 'access content');
}

/**
 * {@inheritdoc}
 */
public
function blockForm($form, FormStateInterface $form_state) {
  $form = parent::blockForm($form, $form_state);
  $config = $this->getConfiguration();

  $form['view_content_text'] = [
    '#type' => 'textfield',
    '#title' => $this->t('View Content CTA text'),
    '#default_value' => $config['view_content_text'] ?? '',
  ];

  $form['view_errata_text'] = [
    '#type' => 'textfield',
    '#title' => $this->t('View Errata CTA text'),
    '#default_value' => $config['view_errata_text'] ?? '',
  ];

  $form['buy_amazon_text'] = [
    '#type' => 'textfield',
    '#title' => $this->t('Buy at Amazon text'),
    '#default_value' => $config['buy_amazon_text'] ?? '',
  ];

  return $form;
}

/**
 * {@inheritdoc}
 */
public
function blockSubmit($form, FormStateInterface $form_state) {
  $values = $form_state->getValues();

  $this->configuration['view_content_text'] = $values['view_content_text'];
  $this->configuration['view_errata_text'] = $values['view_errata_text'];
  $this->configuration['buy_amazon_text'] = $values['buy_amazon_text'];
}
}

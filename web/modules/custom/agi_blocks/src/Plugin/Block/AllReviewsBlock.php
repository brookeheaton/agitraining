<?php

namespace Drupal\agi_blocks\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\agi_blocks\AgiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block with the reviews for a specific course.
 *
 * @Block(
 *   id = "agi_block_all_reviews_block",
 *   admin_label = @Translation("AGI All Reviews Block"),
 * )
 */
class AllReviewsBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * Variable that will store the service.
   *
   * @var \Drupal\agi_blocks\AgiService
   */
  protected $agiService;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account, AgiService $agiService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->account = $account;
    $this->agiService = $agiService;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('agi.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Loading the service into a variable.
    $agi_service = $this->agiService;

    // Getting the current node.
    $current_node = \Drupal::routeMatch()->getParameter('node');

    // Loading the config.
    $config = $this->getConfiguration();

    // Getting the filtered config.
    $block_rating_text = $agi_service->filterConfigText($config['block_rating_text'], $current_node);

    // Getting the info from the reviews.
    $reviews_info = $agi_service->getAllReviewsInfo($current_node);

    return [
      '#theme' => 'agi_all_reviews_blocks',
      '#average_review' => $reviews_info['average'] ?? 0,
      '#number_of_reviews' => $reviews_info['number_of_reviews'] ?? 0,
      '#block_rating_text' => str_replace(['%stars', '%number_of_reviews'], [$reviews_info['average'], $reviews_info['number_of_reviews']], $block_rating_text) ?? '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['block_rating_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Rating text'),
      '#default_value' => $config['block_rating_text'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    //parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();

    $this->configuration['block_rating_text'] = $values['block_rating_text'];
  }
}

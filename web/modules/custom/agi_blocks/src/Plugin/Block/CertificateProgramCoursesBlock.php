<?php

namespace Drupal\agi_blocks\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\agi_blocks\AgiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block that outputs the courses for the Certificate Programs.
 *
 * @Block(
 *   id = "agi_block_certificate_program_courses_block",
 *   admin_label = @Translation("AGI Certificate Program Courses"),
 * )
 */
class CertificateProgramCoursesBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * Variable that will store the service.
   *
   * @var \Drupal\agi_blocks\AgiService
   */
  protected $agiService;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account, AgiService $agiService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->account = $account;
    $this->agiService = $agiService;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('agi.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Loading the service into a variable.
    $agi_service = $this->agiService;

    // Loading the config.
    $config = $this->getConfiguration();

    // Getting the current node.
    $current_node = \Drupal::routeMatch()->getParameter('node');

    // Markup text for the LB.
    $markup_text = t('Placeholder for the "AGI Certificate Program Courses."');

    if ($current_node) {
      // Getting the filered config.
      $block_header = $agi_service->filterConfigText($config['block_header'], $current_node);

      // Getting the courses.
      $courses = $agi_service->getCertificateProgramCourses($current_node);

      return [
        '#theme' => 'agi_certificate_program_courses_block',
        '#courses' => $courses ?? [],
        '#block_header' => $block_header ?? '',
      ];
    }

    // Default for the Layout Builder.
    return [
      '#markup' => $markup_text,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['block_header'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Block header'),
      '#default_value' => $config['block_header'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['block_header'] = $values['block_header'];
  }
}

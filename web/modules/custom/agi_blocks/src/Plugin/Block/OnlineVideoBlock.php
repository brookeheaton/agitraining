<?php

namespace Drupal\agi_blocks\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\agi_blocks\AgiService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block showing a video if the course is online.
 *
 * @Block(
 *   id = "agi_block_online_video_block",
 *   admin_label = @Translation("AGI Online Video Block"),
 * )
 */
class OnlineVideoBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * Variable that will store the service.
   *
   * @var \Drupal\agi_blocks\AgiService
   */
  protected $agiService;

  /**
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Session\AccountInterface $account
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountInterface $account, AgiService $agiService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->account = $account;
    $this->agiService = $agiService;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('agi.service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Loading the service into a variable.
    $agi_service = $this->agiService;

    // Getting the current node.
    $current_node = \Drupal::routeMatch()->getParameter('node');

    // Markup text for the LB.
    $markup_text = t('Placeholder for the "AGI Online Video Block."');

    if ($current_node) {
      // Loading the config.
      $config = $this->getConfiguration();


      switch ($current_node->bundle()) {
        case 'category_location_page':
          // Loading the referenced node.
          $reference_node = Node::load($current_node->field_location_for_cat->target_id);

          // Returning the short name field from the loaded node.
          $city_name = $reference_node->field_city_for_reference_pages->value;
          break;

        case 'seconday_location_category_page':
        case 'course_date':
          $city_name = 'Online';
          break;
        default:
          $city_name = 'Online';
      }

      // Checking if the location is "Online".
      if (isset($city_name) && !empty($city_name) && $city_name === 'Online') {
        return [
          '#theme' => 'agi_online_video_block',
          '#bottom_text' => $config['bottom_text'] ?? $this->t('See how live online training works in this brief video.'),
          '#attached' => [
            'library' => [
              'agi_blocks/video',
            ],
          ],
        ];
      }

      $markup_text = '';

      return [
        '#markup' => $markup_text,
      ];
    }

    // Default for the Layout Builder.
    return [
      '#markup' => $markup_text,
    ];

  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['bottom_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bottom text'),
      '#maxlength' => 1024,
      '#default_value' => $config['bottom_text'] ?? '',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->configuration['bottom_text'] = $values['bottom_text'];
  }
}

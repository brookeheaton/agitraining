(function ($, Drupal) {
  Drupal.behaviors.cart_count = {
    attach: function (context, settings) {
      var cartCount = drupalSettings.cart_count;

      if (parseInt(cartCount) > 0) {
        $("#block-agitraining-secondary-menu")
          .find(".cart-menu-item")
          .addClass("pr-4")
          .parent()
          .append('<span class="badge badge-primary">' + cartCount + "</span>");
      }
    },
  };
})(jQuery, Drupal);

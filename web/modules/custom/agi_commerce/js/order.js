(function ($, Drupal) {
  // Select the first radio on payment.
  $(document).ready(function () {
    $("#edit-payment-information")
      .find(".form-radios")
      .find("input:radio")
      .attr("checked", false);

    $("#edit-payment-information")
      .find(".form-radios")
      .find("input:radio:first")
      .attr("checked", true)
      .trigger("click");
  });

  Drupal.behaviors.order = {
    autoFillStudentInfo: function (firsrName, lastName, email) {
      if ($(".given-name").val() === "") {
        $(".given-name").val(firsrName);
      }

      if ($(".family-name").val() === "") {
        $(".family-name").val(lastName);
      }

      $(".agi-seat-first-name-0").each(function () {
        if ($(this).val() === "") {
          $(this).val(firsrName);
        }
      });

      $(".agi-seat-last-name-0").each(function () {
        if ($(this).val() === "") {
          $(this).val(lastName);
        }
      });

      $(".agi-seat-mail-0").each(function () {
        if ($(this).val() === "") {
          $(this).val(email);
        }
      });
    },
    attach: function (context, settings) {
      // Autofilling the user info for logged in users.
      if (drupalSettings.agi_seats !== null) {
        var studentInfo = drupalSettings.agi_seats;

        // Autofilling the student fields with the logged user information.
        Drupal.behaviors.order.autoFillStudentInfo(
          studentInfo.first_name,
          studentInfo.last_name,
          studentInfo.mail
        );
      } else {
        var self = this;

        $(".given-name").on("blur", function () {
          self.autoFillStudentInfo($(this).val(), "", "");
        });

        $(".family-name").on("blur", function () {
          self.autoFillStudentInfo("", $(this).val(), "");
        });

        $("#edit-contact-information-email").on("blur", function () {
          self.autoFillStudentInfo("", "", $(this).val());
        });
      }
    },
  };
})(jQuery, Drupal);

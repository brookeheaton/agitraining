<?php

namespace Drupal\agi_commerce\Feeds\Target;

use Drupal\feeds\Feeds\Target\Integer;

/**
 * Defines a commerce_price field mapper.
 *
 * @FeedsTarget(
 *   id = "current_usage",
 *   field_types = {"current_usage"}
 * )
 */
class AgiPromotionUsage extends Integer {

}

<?php

namespace Drupal\agi_commerce\Feeds\Target;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\feeds\FieldTargetDefinition;
use Drupal\feeds\Plugin\Type\Target\ConfigurableTargetInterface;
use Drupal\feeds\Plugin\Type\Target\FieldTargetBase;

/**
 * Defines a commerce promotion offer plugin field mapper.
 *
 * @FeedsTarget(
 *   id = "commerce_feeds_plugin_item:commerce_promotion_offer",
 *   field_types = {"commerce_plugin_item:commerce_promotion_offer"}
 * )
 */
class AgiPromotionOffer extends AgiCommercePlugin {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_promotion_offer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return ['target_plugin_id' => 'order_item_fixed_amount_off'];
  }

  public function prepareValue($delta, array &$values) {
    $plugin = $values['target_plugin_id'];
    switch ($plugin) {
      case 'order_percentage_off':
        $percentage = (string) $values['target_plugin_configuration'] / 100;
        $values['target_plugin_configuration'] = ['percentage' => $percentage];
        break;
      case 'order_fixed_amount_off':
        $amount = $values['target_plugin_configuration'];
        $values['target_plugin_configuration'] = [
          'amount' => [
            'number' => $amount,
            'currency_code' => 'USD',
          ],
        ];
        break;
    }
  }

}

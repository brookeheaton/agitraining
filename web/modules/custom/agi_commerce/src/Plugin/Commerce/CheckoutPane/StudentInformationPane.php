<?php

namespace Drupal\agi_commerce\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneInterface;
use Drupal\Core\Form\FormStateInterface;


/**
 * Allows customers to add comments to the order.
 *
 * @CommerceCheckoutPane(
 *   id = "agi_commerce_student_info_pane",
 *   label = @Translation("Student Information"),
 *   default_step = "order_information",
 *   wrapper_element = "fieldset",
 * )
 */
class StudentInformationPane extends CheckoutPaneBase implements CheckoutPaneInterface {

  /**
   * {@inheritdoc}
   */
  public function getOrderItems() {
    $purchased_entities = [];
    $purchased_entities = $this->order->getItems();
    return $purchased_entities;
  }


  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    // Do something custom with the pane form here.
    $user = \Drupal::currentUser();
    $uid = $user->id();
    $user_data = NULL;
    $pane_form['#tree'] = TRUE;
    // Send user data and profile to JS file.
    if (isset($uid) && $uid > 0) {
      $profile = \Drupal::entityTypeManager()
        ->getStorage('profile')
        ->loadByProperties([
          'uid' => $user->id(),
          'type' => 'main',
        ]);
      if (!empty($profile)) {
        $pid = isset($profile[$uid]) ? $profile[$uid] : null;
        if ($pid) {
          $user_data = [
            'mail' => $user->getEmail(),
            'first_name' => !$pid->get('field_first_name')
              ->isEmpty() ? $pid->get('field_first_name')
              ->first()
              ->getString() : NULL,
            'last_name' => !$pid->get('field_last_name')
              ->isEmpty() ? $pid->get('field_last_name')
              ->first()
              ->getString() : NULL,
            'pc_or_mac' => !$pid->get('field_mac_or_pc')
              ->isEmpty() ? $pid->get('field_mac_or_pc')
              ->first()
              ->getString() : NULL,
          ];
        }
      }
    }
    $pane_form['#attached']['drupalSettings']['agi_seats'] = $user_data;
    $pane_form['agi_seats'] = [
      '#title' => 'data',
      '#type' => 'hidden',
      '#value' => $user_data,
      '#weight' => -1,
    ];
    // Get line items from current order.
    $order_items = $this->getOrderItems();
    if (!empty($order_items)) {
      foreach ($order_items as $order_item) {
        $oid = $order_item->id();
        $quantity = $order_item->getQuantity();
        $course = $order_item->getPurchasedEntity();
        if (!empty($course)) {
          // Create new form.
          $pane_form[$oid]['users_entry'] = [
            '#type' => 'fieldset',
            '#title' => t('Students Information'),
            '#prefix' => '<div class="users_entry">',
            '#suffix' => '</div>',
            '#description' => t('Please enter all students information'),
            '#collapsible' => FALSE,
            '#description_display' => 'before',
            '#attributes' => ['class' => ['row']],
          ];
          $pane_form[$oid]['users_entry']['title'] = [
            '#type' => 'item',
            '#markup' => '<h5 class="col-lg-12">' . $course->label() . '</h5>',
            '#weight' => 0,
            '#attributes' => [
              'class' => ['row'],
            ],
          ];
          // Print fields related to seats.
          for ($i = 0; $i < $quantity; $i++) {
            $pane_form[$oid]['users_entry'][$i] = [
              '#type' => 'item',
              '#attributes' => [
                'class' => ['row'],
              ],
            ];
            $pane_form[$oid]['users_entry'][$i]['class'] = [
              '#type' => 'item',
              '#value' => $oid,
              '#attributes' => [
                'class' => ['row'],
              ],
            ];
            $pane_form[$oid]['users_entry'][$i]['field_first_name'] = [
              '#type' => 'textfield',
              '#required' => TRUE,
              '#weight' => 1,
              '#title' => t('First Name'),
              '#size' => 30,
              '#maxlength' => 128,
              '#prefix' => '<div class="agi-seats-field first col-lg-3">',
              '#suffix' => '</div>',
              '#attributes' => ['class' => ['agi-seat-first-name-' . $i]],
            ];
            $pane_form[$oid]['users_entry'][$i]['field_last_name'] = [
              '#type' => 'textfield',
              '#required' => TRUE,
              '#weight' => 2,
              '#title' => t('Last Name'),
              '#size' => 30,
              '#maxlength' => 128,
              '#prefix' => '<div class="agi-seats-field last col-lg-3">',
              '#suffix' => '</div>',
              '#attributes' => ['class' => ['agi-seat-last-name-' . $i]],
            ];
            $pane_form[$oid]['users_entry'][$i]['mail'] = [
              '#type' => 'textfield',
              '#required' => TRUE,
              '#weight' => 2,
              '#title' => t('Email'),
              '#size' => 30,
              '#maxlength' => 128,
              '#prefix' => '<div class="agi-seats-field email col-lg-3">',
              '#suffix' => '</div>',
              '#attributes' => ['class' => ['agi-seat-mail-' . $i]],
              '#element_validate' => ['agi_commerce_email_validate'],
            ];

            $options = [
              'Mac' => t('Mac'),
              'Windows' => t('Windows'),
              'No preference' => t('No preference / Uncertain'),
            ];
            $pane_form[$oid]['users_entry'][$i]['field_mac_or_pc'] = [
              '#type' => 'radios',
              '#title' => t('Choose an Option'),
              '#options' => $options,
              '#weight' => 4,
              '#prefix' => '<div class="agi-seats-field pc col-lg-3">',
              '#suffix' => '</div>',
              '#attributes' => ['class' => ['agi-seat-pc-or-mac-' . $i]],
            ];
          }
        }
      }
      $pane_form[$oid]['online_materials'] = [
        '#type' => 'textarea',
        '#title' => $this->t('For online courses only, please indicate if you wish to have materials sent to a different address than your billing address.'),
        '#description' => $this->t('For in-person classes, your materials will be provided to you at our training center.'),
      ];
    }
    return $pane_form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    // Get seats values.
    $values = $form_state->getValues();
    // Get an oder_item storage object.
    $order_item_storage = \Drupal::entityTypeManager()
      ->getStorage('commerce_order_item');
    if (isset($values["agi_commerce_student_info_pane"])) {
      foreach ($values["agi_commerce_student_info_pane"] as $oid => $entry) {
        if (is_numeric($oid)) {
          $user_ids = [];
          $users = $entry["users_entry"];
          // Load order item.
          $order_item = $order_item_storage->load($oid);
          // Get the users set as students on the seats.
          foreach ($users as $user) {
            if (isset($user['mail']) && !empty($user['mail'])) {
              // Check if user account exists.
              $existing_user = user_load_by_mail($user['mail']);
              if (is_object($existing_user)) {
                // Create profile or update.
                $profile = agi_commerce_verify_profile_create($existing_user, $user);
              }
              else {
                // Create profile and user.
                $existing_user = agi_commerce_create_user($user);
              }
              if (is_object($existing_user)) {
                $user_ids[] = $existing_user->id();
              }
            }
          }
          if (!empty($entry['online_materials'])) {
            $order = $order_item->getOrder();
            $order->field_field_materials_address = $entry['online_materials'];
          }
          if (!empty($user_ids)) {
            foreach ($user_ids as $user) {
              // Associate users with the order item
              $order_item->field_user[] = ['target_id' => $user];
            }
            $order_item->save();
          }
        }
      }
    }
  }

}

<?php

namespace Drupal\agi_commerce\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Remove courses prior to current date from carts
 *
 * @QueueWorker(
 *  id = "agi_commerce_cart_cleanup",
 *  title = @Translation("Cart cleanup"),
 *  cron = {"time" = 30}
 * )
 */
class CartCleanup extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The order storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderStorage;

  /**
   * The order type storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderTypeStorage;

  /**
   * Constructs a new CartExpiration object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->orderStorage = $entity_type_manager->getStorage('commerce_order');
    $this->orderTypeStorage = $entity_type_manager->getStorage('commerce_order_type');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    foreach ($data as $order_id) {
      $order = $this->orderStorage->load($order_id);
      if ($order) {
        $order_items = $order->getItems();
        foreach ($order_items as $item) {
          $product_variation = $item->getPurchasedEntity();
          if ($product_variation && $product_variation->hasField('field_event_start_date') && !$product_variation->field_event_start_date->isEmpty()) {
            $start_date = $product_variation->get('field_event_start_date')->value;
            $current_date = new DrupalDateTime('now');
            $course_date = new DrupalDateTime($start_date, 'UTC');
            $course_date_timestamp = $course_date->getTimestamp();
            $now_timestamp = $current_date->getTimestamp();
            if ($now_timestamp > $course_date_timestamp) {
              $order->removeItem($item);
            }
          }
        }
      }
    }
  }

}

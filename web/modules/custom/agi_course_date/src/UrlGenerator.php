<?php

namespace Drupal\agi_course_date;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\node\Entity\Node;
use Drupal\pathauto\PathautoState;

/**
 * UrlGenerator service.
 */
class UrlGenerator {

  /**
   * Generates a path pattern alias for Course Date based on related 'Event' Product data
   */
  public function generateUrl(Node $node) {
    $product_variation = $node->get('field_cd_event')->first()->get('entity')->getTarget()->getValue();
    $location = $product_variation
      ->get('field_event_location')
      ->first()
      ->get('entity')
      ->getTarget()
      ->getValue();
    $location_name = $location->get('field_location_name')->getString();
    // Set the location url value
    switch ($location_name) {
      case 'New York':
        $location_value = 'new-york';
        break;

      case 'Philadelphia':
        $location_value = 'philadelphia';
        break;

      case 'Boston':
        $location_value = 'boston';
        break;

      default:
        $location_value = 'online';
    }
    // Get the Parent Course Name
    $course_title_raw = $product_variation->get('field_event_name')
      ->first()
      ->get('entity')
      ->getTarget()
      ->getValue()
      ->label();
    $course_title = str_replace(' ', '-', $course_title_raw);
    $course_title = str_replace('--', '', $course_title);
    $course_title = strtolower(preg_replace("/[^A-Za-z0-9]/", "-", $course_title));
    // Get the Parent Course ID and alias
    $course_id = $product_variation->get('field_event_name')
      ->first()
      ->getValue()['target_id'];
    $course_path_alias = \Drupal::service('path_alias.manager')
      ->getAliasByPath('/node/' . $course_id);
    // Remove levels from the alias
    $course_path_alias = str_replace('/introduction', '', $course_path_alias);
    $course_path_alias = str_replace('/intermediate', '', $course_path_alias);
    $course_path_alias = str_replace('/advanced', '', $course_path_alias);
    // Get the event date for use in setting the path alias.
    $event_date = $product_variation->field_event_start_date->value;
    $timestamp = date("U", strtotime($event_date));
    $date = DrupalDateTime::createFromTimestamp($timestamp);
    $date_format = strtolower($date->format('F-d-Y'));
    // Set the path alias
    $alias = $course_path_alias . '/' . $location_value . '/' . $course_title . '-' . $date_format;;
    $node->path = [
      'alias' => $alias,
      'pathauto' => PathautoState::SKIP,
    ];
    return $node;
  }
}

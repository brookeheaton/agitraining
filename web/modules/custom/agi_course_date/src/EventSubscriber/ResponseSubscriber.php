<?php

namespace Drupal\agi_course_date\EventSubscriber;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\node\Entity\Node;

/**
 * Class ResponseSubscriber.
 * Subscribe drupal events.
 *
 * @package Drupal\agi_course_date
 */
class ResponseSubscriber extends HttpExceptionSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats() {
    return ['html'];
  }

  /**
   * Redirect if 403 and node an event.
   *
   * @param FilterResponseEvent $event
   *   The route building event.
   */
  public function on403(GetResponseForExceptionEvent $event) {
    $nid = \Drupal::routeMatch()->getRawParameter('node');
    $current_node = NULL;

    if ($nid) {
      $current_node = Node::load($nid);
    }

    if ($current_node instanceof Node && $current_node->getType() == 'course_date' && !$current_node->isPublished()) {
      // Current node alias.
      $parent_category_nid = $current_node->field_cd_event->entity->field_event_name->entity->field_primary_category->first()->getString();

      $destination = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $parent_category_nid);

      // Making the redirect happen.
      $metadata = CacheableMetadata::createFromObject($current_node)->addCacheTags(['rendered']);
      $response = new TrustedRedirectResponse($destination, 301);
      $response->addCacheableDependency($metadata);
      $response->setCache(['max_age' => 0]);
      $event->setResponse($response);
    }
  }
}

<?php

namespace Drupal\agi_course_date\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\feeds\Event\FeedsEvents;
use Drupal\feeds\Event\EntityEvent;
use Drupal\node\Entity\Node;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\commerce_product\Entity\Product;
use Drupal\path_alias\Entity\PathAlias;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\agi_course_date\EventSubscriber
 */
class FeedsImportEntity implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      FeedsEvents::PROCESS_ENTITY_POSTSAVE => 'postSave',
      FeedsEvents::CLEAN => 'delete',
    ];
  }

  /**
   * React to an entity being saved.
   *
   * @param \Drupal\feeds\Event\EntityEvent $event
   *   Entity event.
   */
  public function postSave(EntityEvent $event) {
    $entity_type = $event->getEntity()->getEntityTypeId();
    if ($entity_type === 'commerce_product_variation') {
      // Get the imported Product Variation
      $product_variation = $event->getEntity();
      // Returns a Drupal\Core\Datetime\DateFormatter object.
      $product_generator = \Drupal::service('agi_course_date.parent_product_generator');
      $product = $product_generator->generateParentProduct($product_variation);

      // Get the Product Variation title
      $product_variation_title = $product_variation->label();
      // Get the Product Variation location title.
      $location = $product_variation
        ->get('field_event_location')
        ->first()
        ->get('entity')
        ->getTarget()
        ->getValue();
      $location_name = $location->get('field_location_name')->getString();
      // Set the location url value
      switch ($location_name) {
        case 'New York':
          $location_value = 'new-york';
          break;

        case 'Philadelphia':
          $location_value = 'philadelphia';
          break;

        case 'Boston':
          $location_value = 'boston';
          break;

        default:
          $location_value = 'online';
      }
      // Get the Parent Course Name
      $course_title_raw = $product_variation->get('field_event_name')
        ->first()
        ->get('entity')
        ->getTarget()
        ->getValue()
        ->label();
      $course_title = str_replace(' ', '-', $course_title_raw);
      $course_title = str_replace('--', '', $course_title);
      $course_title = strtolower(preg_replace("/[^A-Za-z0-9]/", "-", $course_title));
      // Get the Parent Course ID and alias
      $course_id = $product_variation->get('field_event_name')
        ->first()
        ->getValue()['target_id'];
      $course_path_alias = \Drupal::service('path_alias.manager')
        ->getAliasByPath('/node/' . $course_id);
      // Remove levels from the alias
      $course_path_alias = str_replace('/introduction', '', $course_path_alias);
      $course_path_alias = str_replace('/intermediate', '', $course_path_alias);
      $course_path_alias = str_replace('/advanced', '', $course_path_alias);
      // Get the event date for use in setting the path alias.
      $event_date = $product_variation->field_event_start_date->value;
      $timestamp = date("U", strtotime($event_date));
      $date = DrupalDateTime::createFromTimestamp($timestamp);
      $date_format = strtolower($date->format('F-d-Y'));
      // Create the related Course Date node for this Product Variation
      $node = Node::create([
        'type' => 'course_date',
        'title' => "$product_variation_title",
        'field_cd_event' => [
          'target_id' => $product_variation->id(),
        ],
        'uid' => 1,
        'created' => \Drupal::time()->getRequestTime(),
        'changed' => \Drupal::time()->getRequestTime(),
      ]);
      $node->path->pathauto = 1;
      $node->save();
      // Set the path alias
      $node->setPublished(TRUE);
      $node->save();
      $path_alias = PathAlias::create([
        'path' => '/node/' . $node->id(),
        'alias' => $course_path_alias . '/' . $location_value . '/' . $course_title . '-' . $date_format,
      ]);
      $path_alias->save();
      $course_date_title = $node->label();
      $message = "Created Course Date: $course_date_title\r";
      \Drupal::messenger()->addStatus($message);
      \Drupal::logger('agi_course_date')->notice($message);
    }
  }
}

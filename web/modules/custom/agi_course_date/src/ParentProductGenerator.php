<?php

namespace Drupal\agi_course_date;

use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductVariation;

/**
 * ParentProductGenerator service.
 */
class ParentProductGenerator {

  /**
   * Generates a Parent Commerce Product for a Variation
   *
   * @param \Drupal\commerce_product\Entity\ProductVariation $product_variation
   */
  public function generateParentProduct(ProductVariation $product_variation) {
    $t = 1;
      // Create a Product to host the Variation
      $product = Product::create([
        'type' => 'event',
        'title' => t($product_variation->label()),
        'variations' => [$product_variation->id()],
      ]);
      $product->save();
      $message = "Created Course Product for: $product_variation->label()\r";
      \Drupal::messenger()->addStatus($message);
      \Drupal::logger('agi_course_date')->notice($message);
      return $product;
  }
}

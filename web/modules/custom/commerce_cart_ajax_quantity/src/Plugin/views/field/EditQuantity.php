<?php

namespace Drupal\commerce_cart_ajax_quantity\Plugin\views\field;

use Drupal\commerce_cart\Plugin\views\field\EditQuantity as EditQuantityBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form element for editing the order item quantity.
 *
 * @ViewsField("commerce_cart_ajax_quantity_views_item_edit_quantity")
 */
class EditQuantity extends EditQuantityBase {

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    parent::viewsForm($form, $form_state);
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;
    // The view is empty, abort.
    if (empty($this->view->result)) {
      unset($form['actions']);
      return;
    }

    $form['#attached'] = [
      'library' => ['commerce_cart/cart_form'],
    ];
    $form[$this->options['id']]['#tree'] = TRUE;

    // Views result in the form to retrieve in the ajax request.
    $form['view_result'] = [
      '#type' => 'value',
      '#value' => $this->view->result,
    ];

    foreach ($this->view->result as $row_index => $row) {
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->getEntity($row);
      if ($this->options['allow_decimal']) {
        $form_display = commerce_get_entity_display('commerce_order_item', $order_item->bundle(), 'form');
        $quantity_component = $form_display->getComponent('quantity');
        $step = $quantity_component['settings']['step'];
        $precision = $step >= '1' ? 0 : strlen($step) - 2;
      }
      else {
        $step = 1;
        $precision = 0;
      }

      if (!$order_item->isLocked()) {
        $form[$this->options['id']][$row_index] = [
          '#type' => 'number',
          '#title' => $this->t('Quantity'),
          '#title_display' => 'invisible',
          '#default_value' => round($order_item->getQuantity(), $precision),
          '#size' => 4,
          '#min' => 0,
          '#max' => 9999,
          '#step' => $step,
          '#required' => TRUE,
          '#attributes' => [
            'class' => [
              'quantity-edit-input',
            ],
          ],
          '#show_update_message' => TRUE,
          '#ajax' => [
            'callback' => [static::class, 'ajaxRefreshSummary'],
            'keypress' => TRUE,
            'event' => 'change',
            'effect' => 'fade',
            'prevent' => 'sumbit',
          ],
        ];
      }
      else {
        $form[$this->options['id']][$row_index] = [
          '#type' => 'item',
          '#plain_text' => round($order_item->getQuantity(), $precision),
        ];
      }
    }

    $form['actions']['submit']['#update_cart'] = FALSE;
    $form['actions']['submit']['#show_update_message'] = TRUE;
    // Replace the form submit button label.
    $form['actions']['submit']['#value'] = $this->t('Update cart');

    // Hide Update Cart Button
    unset($form['actions']['submit']);

  }


  /**
   * Ajax callback function to refresh cart item summary on change of quantity field.
   */
  public static function ajaxRefreshSummary(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $triggering_element = $form_state->getTriggeringElement();
    /** @var CartProviderInterface $cpi */
    $cartProvider = \Drupal::service('commerce_cart.cart_provider');
    $cart_manager = \Drupal::service('commerce_cart.cart_manager');
    /** @var \Drupal\commerce_order\Entity\OrderInterface[] $carts */
    $carts = $cartProvider->getCarts();
    $carts = array_filter($carts, function ($cart) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
      // There is a chance the cart may have converted from a draft order, but
      // is still in session. Such as just completing check out. So we verify
      // that the cart is still a cart.
      return $cart->cart->value;
    });

    $order = reset($carts);
    $quantities = $form_state->getValue('edit_quantity', []);
    $view_result = $form_state->getValue('view_result', []);

    $save_cart = FALSE;

    foreach ($quantities as $row_index => $quantity) {
      if (is_numeric($quantity) && floor($quantity) != $quantity) {
        continue;
      }
      if (!is_numeric($quantity) || $quantity < 0) {
        // The input might be invalid if the #required or #min attributes
        // were removed by an alter hook.
        continue;
      }

      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $resultRow = $view_result[$row_index];
      $order_item = $resultRow->_relationship_entities['order_items'];
      $order_id = $order_item->get('order_id')->target_id;
      if ($order_item->getQuantity() == $quantity) {
        // The quantity hasn't changed.
        continue;
      }

      if ($quantity > 0) {
        $order_item->setQuantity($quantity);
        $cart_manager->updateOrderItem($order, $order_item, FALSE);
      }
      else {
        // Treat quantity "0" as a request for deletion.
        $cart_manager->removeOrderItem($order, $order_item, FALSE);
        $response->addCommand(new RemoveCommand('#views-form-commerce-cart-form-default-' . $order_id));
      }

      $order_item_total = $order_item->get('total_price')->view([
        'label' => 'hidden',
        'type' => 'commerce_order_total_summary',
      ]);

      $response->addCommand(new HtmlCommand('#views-form-commerce-cart-form-default-' . $order_id . ' .quantity .cart-body', '<b>' . $quantity . '</b>'));
      $response->addCommand(new HtmlCommand('#views-form-commerce-cart-form-default-' . $order_id . ' .total-price__number .cart-body', '<b>' . \Drupal::service('renderer')->render($order_item_total) . '</b>'));
      $save_cart = TRUE;
    }

    if ($save_cart) {
      $order->save();
      if (!empty($triggering_element['#show_update_message'])) {
        \Drupal::messenger()->addMessage('Your shopping cart has been updated');
      }
    }

    $order_total = $order->get('total_price')->view([
      'label' => 'hidden',
      'type' => 'commerce_order_total_summary',
    ]);
    $order_total['#prefix'] = '<div data-drupal-selector="order-total-summary">';
    $order_total['#suffix'] = '</div>';

    $rendered_total = \Drupal::service('renderer')->render($order_total);

    if (isset($order_total)) {
      $response->addCommand(new ReplaceCommand('#views-form-commerce-cart-form-default-' . $order_id . ' [data-drupal-selector="order-total-summary"]', $rendered_total));
    }

    $status_messages = ['#type' => 'status_messages'];
    $messages = \Drupal::service('renderer')->renderRoot($status_messages);
    if (!empty($messages)) {
      $response->addCommand(new HtmlCommand('.highlighted', $messages));
      $response->addCommand(new InvokeCommand('.toast.fade', 'addClass', ['show']));
    }

    return $response;
  }
}

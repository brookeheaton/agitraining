<?php

namespace Drupal\agi_schema;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\file\Entity\File;

/**
 * Class that define the service to get schema info.
 */
class AgiSchemaService {

  /**
   * Getting the Product Image for the node.
   */
  public function getProductImage($node) {
    $base_path = \Drupal::request()->getSchemeAndHttpHost();
    $image_path = $base_path . '/' . \Drupal::theme()->getActiveTheme()->getLogo();

    if ($node instanceof NodeInterface) {
      // Loading the referenced node.
      switch ($node->bundle()) {
        case 'category_location_page':
          $image_fid = $node->field_location_for_cat->entity->field_primary_image_slides_media->target_id;
          break;
        case 'location':
          $image_fid = $node->field_primary_image_slides_media->target_id;
          break;
      }

      if (!empty($image_fid)) {
        $file = $image_fid ? File::load($image_fid)->getFileUri() : NULL;
        $image_path = $base_path . '/' .  \Drupal::service('file_url_generator')->generate($file)->toString();
      }
    }

    // Returning the image path.
    return $image_path;
  }

  /**
   * Getting the Location City Short Name.
   */
  public function getCategoryLogo($current_node) {
    if ($current_node instanceof NodeInterface && $current_node->hasField('field_right_header_image')) {
      $fid = $current_node->field_left_header_image->target_id;
      if ($fid) {
        $file = File::load($fid);
      }
      if ($file) {
        $variables = [
          'responsive_image_style_id' => 'category_logo',
          'uri' => $file->getFileUri(),
        ];
      }
      $image = \Drupal::service('image.factory')->get($file->getFileUri());
      if ($image->isValid()) {
        $variables['width'] = $image->getWidth();
        $variables['height'] = $image->getHeight();
      }
      else {
        $variables['width'] = $variables['height'] = NULL;
      }
      $logo_build = [
        '#theme' => 'responsive_image',
        '#width' => $variables['width'],
        '#height' => $variables['height'],
        '#responsive_image_style_id' => $variables['responsive_image_style_id'],
        '#uri' => $variables['uri'],
      ];

      $renderer = \Drupal::service('renderer');
      $renderer->addCacheableDependency($logo_build, $file);

      // Returning the shor name field from the loaded node.
      $responsive_image = $renderer->renderRoot($logo_build);
      return $responsive_image;
    }
    else {
      return '';
    }
  }

}

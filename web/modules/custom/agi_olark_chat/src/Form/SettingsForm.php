<?php

namespace Drupal\agi_olark_chat\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure AGI Olark Chat settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'agi_olark_chat_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['agi_olark_chat.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['olark_identity'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Olark Identity'),
      '#default_value' => $this->config('agi_olark_chat.settings')->get('olark_identity'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('agi_olark_chat.settings')
      ->set('olark_identity', $form_state->getValue('olark_identity'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}

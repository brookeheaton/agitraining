<?php

namespace Drupal\agi_olark_chat\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an Olark Chat block.
 *
 * @Block(
 *   id = "agi_olark_chat_block",
 *   admin_label = @Translation("Olark Chat Block"),
 *   category = @Translation("AGI Olark Chat")
 * )
 */
class OlarkBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
   $config = \Drupal::config('agi_olar_chat.settings');
   $olark_identity = $config->get('olark_identity');
    return [
      '#markup' => $this->t('<div id="agi_olark_chat"></div>'),
      '#title' => '',
      '#attached' => [
        'library' => [
          'agi_olark_chat/agi_olark_chat',
        ],
        'drupalSettings' => [
         'olark_chat_identity' => $olark_identity ?? '',
          ],
      ],
    ];
  }

}

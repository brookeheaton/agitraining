<?php

namespace Drupal\agi_migrate\Feeds\Target;

use Drupal\feeds\Feeds\Target\StringTarget;
use Drupal\migrate\Plugin\migrate\destination\Entity;

/**
 * Defines a commerce_price field mapper.
 *
 * @FeedsTarget(
 *   id = "commerce_plugin_item:commerce_promotion_offer",
 *   field_types = {"commerce_plugin_item:commerce_promotion_offer"}
 * )
 */
class CommercePromotionOffer extends StringTarget {

}

<?php

namespace Drupal\agi_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Provides a 'Ipaddress' migrate process plugin.
 *
 * @MigrateProcessPlugin(
 *  id = "ipaddress"
 * )
 */
class Ipaddress extends ProcessPluginBase {


  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    // Instantiate our IP, will throw \Exception if invalid.
    try {
      $long = long2ip($value);
      $ip_address = inet_pton($long);
    } catch (\Exception $e) {
      $message = "Ip Address is invalid.";
      \Drupal::logger('agi_migrate')->notice($message);
      return NULL;
    }
    return $ip_address;
  }

}

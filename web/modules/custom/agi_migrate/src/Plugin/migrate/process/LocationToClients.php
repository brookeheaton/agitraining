<?php

namespace Drupal\agi_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Provides a 'CategoryToClients' migrate process plugin.
 *
 * @MigrateProcessPlugin(
 *  id = "location_to_clients"
 * )
 */
class LocationToClients extends ProcessPluginBase {


  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $location_clients = [
      "AGI Training Philadelphia" => "Philadelphia Inquirer, Philadelphia Magazine, PNC bank, Comcast, and the many universities, colleges, and agencies located in and around Philadelphia",
      "AGI Training Boston" => "The Boston Globe, Chewy, Fidelity, MIT, Harvard University, MGH (Partners Healthcare), Staples and hundreds of other Boston-area organizations, companies, and schools",
      "Online" => "Washington Post, New York Times, Dior, LVMH, Fidelity Investments, Chewy, and hundreds of other companies, schools, and organizations"
      ];
    $clients = $location_clients[$value];
    if ($clients) {
      return $clients;
    }
    return NULL;
  }

}

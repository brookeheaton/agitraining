<?php

namespace Drupal\agi_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;
use Drupal\Core\Database\Database;

/**
 * Provides a 'DeriveAltText' migrate process plugin.
 *
 * @MigrateProcessPlugin(
 *  id = "derive_alt_text"
 * )
 */
class DeriveAltText extends ProcessPluginBase {


  /**
   * {@inheritdoc}
   */
  public function transform($fid, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $filename = $row->getSource()["filename"];
    $results = NULL;
    $alt_text = NULL;
    $database = Database::getConnection('default', 'migrate');
    $alt_text_fields = [
      'field_brafton_image',
      'field_cat_hero_image',
      'field_dc_book_image',
      'field_featured_image',
      'field_location_image',
      'field_primary_image_slides',
      'field_left_header_image',
      'field_right_header_image',
      'field_staff_image'
    ];
    foreach ($alt_text_fields as $alt_text_field) {
      $query = $database->select("field_data_" . $alt_text_field, 'field');
      $query->fields('field', ["$alt_text_field" . "_alt"]);
      $query->condition('entity_id', $fid);
      $results = $query->execute()->fetchField();
      if (!empty($results) && is_string($results)) {
        $alt_text = $results;
      }
    }
    if ($alt_text) {
      $message = "Alt text for file $fid ($filename) is \"$alt_text\", derived from $alt_text_field.";
      \Drupal::logger('agi_migrate')->notice($message);
      return $alt_text;
    }
    else {
      $message = "No alt text found for file  $fid ($filename).";
      \Drupal::logger('agi_migrate')->notice($message);
      return NULL;
    }
  }

}

<?php

namespace Drupal\agi_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Provides a 'CategoryToIndustriesServed' migrate process plugin.
 *
 * @MigrateProcessPlugin(
 *  id = "category_to_industries_served"
 * )
 */
class CategoryToIndustriesServed extends ProcessPluginBase {

  /**
   * Utility: find term by name and vid.
   *
   * @param null $name
   *  Term name
   * @param null $vid
   *  Term vid
   *
   * @return int
   *  Term id or 0 if none.
   */
  protected function getTidByName($name = NULL, $vid = NULL) {
    $properties = [];
    if (!empty($name)) {
      $properties['name'] = $name;
    }
    if (!empty($vid)) {
      $properties['vid'] = $vid;
    }
    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);
    $term = reset($terms);

    return !empty($term) ? $term->id() : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $category_industries = [
      "3DS Max Training Classes" => ['3D Design', '3D Modeling'],
      "Acrobat and PDF for Prepress Training Classes" => [
        'Adobe',
        'digital documents',
        'digital forms',
        'PDF',
      ],
      "Acrobat Certification Training" => [
        'Adobe',
        'digital documents',
        'digital forms',
        'PDF',
      ],
      "Adobe Acrobat Capture Training" => [
        'Adobe',
        'digital documents',
        'digital forms',
        'PDF',
      ],
      "Adobe Acrobat Classes" => [
        'Adobe',
        'digital documents',
        'digital forms',
        'PDF',
        'Section 508',
        'accessibility',
      ],
      "Adobe Animate Training Classes" => ['animation', 'web design'],
      "Adobe Captivate Training" => [
        'Adobe',
        'e-learning',
        'online learning',
        'digital learning',
      ],
      "Adobe Classes and Adobe Courses" => [
        'Adobe',
        'Creative Cloud',
        'digital imaging',
        'graphic design',
        'page layout',
        'video editing',
        'animation',
      ],
      "Adobe Connect Training Classes" => [
        'Adobe',
        'e-learning',
        'online learning',
      ],
      "Adobe Creative Cloud Classes" => [
        'Adobe',
        'Creative Cloud',
        'digital imaging',
        'graphic design',
        'page layout',
        'video editing',
        'animation',
      ],
      "Adobe Creative Suite 5 Training Classes" => ['Adobe'],
      "Adobe CS6 Training Classes" => ['Adobe'],
      "Adobe Digital Publishing Suite (DPS) Training" => [
        'Adobe',
        'digital publishing',
      ],
      "Adobe Edge Training Classes" => [
        'Adobe',
        'Creative Cloud',
        'animation',
        'web design',
      ],
      "Adobe Experience Manager Forms Training" => [
        'Adobe',
        'digital documents',
        'digital forms',
        'PDF',
      ],
      "Adobe Experience Manager Training" => ['Adobe'],
      "Adobe Fireworks Training Classes" => [
        'Adobe',
        'Creative Cloud',
        'prototyping',
        'web design',
      ],
      "Adobe Illustrator Classes" => ['Adobe', 'Creative Cloud'],
      "Adobe Illustrator for Fashion & Apparel Design Training Classes" => [
        'Adobe',
        'Creative Cloud',
        'fashion design',
        'Illustration',
      ],
      "Adobe Illustrator for Shoe and Footwear Design Training Classes" => [
        'Adobe',
        'Creative Cloud',
        'shoe design',
      ],
      "Adobe LiveCycle Designer Training Classes" => [
        'Adobe',
        'PDF',
        'forms design',
      ],
      "Adobe Muse Training Classes" => [
        'Adobe',
        'Creative Cloud',
        'web design',
      ],
      "Adobe Premiere Pro Classes and Courses" => [
        'Adobe',
        'Adobe Certification',
      ],
      "Adobe Presenter classes" => [
        'Adobe',
        'online learning',
        'presentation design',
      ],
      "Adobe XD Training" => [
        'Adobe',
        'Creative Cloud',
        'UX design',
        'user experience',
        'x',
      ],
      "After Effects Certification Training" => [
        'Adobe',
        'Creative Cloud',
        'video editing',
        'animation',
        'special effects',
        'motion graphics',
      ],
      "After Effects Classes & Courses" => [
        'video editing',
        'animation',
        'special effects',
        'motion graphics',
      ],
      "Apple Training Classes" => [
        'graphic design',
        'visual design',
        'video editing',
        'slide design',
      ],
      "Camtasia Training" => [
        'e-learning',
        'online learning',
        'digital learning',
      ],
      "Captivate Certification Training" => [
        'Adobe',
        'e-learning',
        'online learning',
      ],
      "Capture One Training" => ['digital imaging', 'photography'],
      "Clone of Workforce Training Fund Courses" => [''],
      "Coding classes for high schools students" => [
        'web coding',
        'web development',
      ],
      "Coldfusion Training Classes" => [
        'Adobe',
        'web coding',
        'web development',
      ],
      "Creative Cloud Certification Training" => [
        'Adobe',
        'Creative Cloud',
        'Illustration',
        'digital imaging',
        'graphic design',
        'page layout',
        'video editing',
        'motion graphics',
      ],
      "Creative Cloud Workshops in Boston" => [
        'Adobe',
        'Creative Cloud',
        'Illustration',
        'page layout',
      ],
      "CSS Training Classes" => [
        'web coding',
        'HTML & CSS',
        'web design',
        'web development',
      ],
      "Digital Marketing Classes" => [
        'data visualization',
        'visual design',
        'HTML email',
        'email marketing',
        'Excel',
        'data analytics',
        'data analysis',
      ],
      "Digital Publishing Suite (DPS) Certification Training" => ['Adobe'],
      "Dreamweaver Certification Training" => [
        'Adobe',
        'HTML & CSS',
        'web design',
      ],
      "Dreamweaver Training Classes" => ['Adobe', 'HTML & CSS', 'web design'],
      "Drupal Training Classes" => [
        'web coding',
        'web design',
        'web development',
      ],
      "e-learning classes" => [
        'e-learning',
        'online learning',
        'digital learning',
      ],
      "eBook Training Classes" => [
        'Adobe',
        'digital publishing',
        'ebook',
        'epub',
      ],
      "Edge Animate Certification Training" => [
        'Adobe',
        'Creative Cloud',
        'animation',
      ],
      "Email Marketing Courses" => [
        'HTML email',
        'email marketing',
        'digital marketing',
      ],
      "Encore Training Classes" => ['video editing'],
      "Enfocus Pitstop Training Classes" => ['Adobe', 'digital forms'],
      "Excel Classes" => [
        'data visualization',
        'Excel',
        'data analysis',
        'spreadsheets',
        'Microsoft Office',
      ],
      "Final Cut Pro Training Classes" => [
        'Apple',
        'video editing',
        'special effects',
      ],
      "Flash (Animate) Certification Training" => [
        'Adobe',
        'Creative Cloud',
        'animation',
      ],
      "Flex Training Classes" => ['web coding', 'web development'],
      "Google Analytics Classes" => [
        'digital marketing',
        'web analytics',
        'data analytics',
      ],
      "Google Docs Training Classes" => [
        'digital documents',
        'digital publishing',
        'slide design',
        'spreadsheets',
        'forms design',
      ],
      "Google Tag Manager Training Classes" => [
        'digital marketing',
        'web analytics',
        'data analysis',
      ],
      "Graphic Design Classes" => [
        'Creative Cloud',
        'data visualization',
        'graphic design',
        'visual design',
      ],
      "Graphic Design Courses for High School Students" => [
        'Illustration',
        'graphic design',
        'visual design',
        'page layout',
      ],
      "HTML Classes" => [
        'web coding',
        'HTML & CSS',
        'web design',
        'web development',
      ],
      "HTML Email Training Classes" => [
        'HTML email',
        'email marketing',
        'digital marketing',
        'HTML & CSS',
      ],
      "HTML5 Classes" => ['web coding', 'HTML & CSS', 'web development'],
      "Illustrator Certification Training" => [
        'Adobe',
        'Adobe Certification',
        'Illustration',
      ],
      "InDesign Certification Training" => ['Adobe', 'Adobe Certification'],
      "InDesign Classes and Courses" => [
        'Adobe',
        'Creative Cloud',
        'digital publishing',
        'page layout',
      ],
      "iOS Development Training" => ['Apple', 'app development'],
      "iPad Training Classes" => ['Apple', 'e-learning', 'digital learning'],
      "JavaScript Training Classes" => [
        'web coding',
        'HTML & CSS',
        'web development',
      ],
      "Keynote Training Classes" => [
        'data visualization',
        'slide design',
        'presentation design',
      ],
      "Maya Classes" => [
        '3D Design',
        '3D Modeling',
        'animation',
        'special effects',
        'motion graphics',
      ],
      "Microsoft Access Classes" => [
        'data visualization',
        'Microsoft Office',
        'databases',
      ],
      "Microsoft Blend for Visual Studio Training Classes" => ['web development'],
      "Microsoft Expression Web Training Classes" => ['web development'],
      "Microsoft Office Classes" => ['Excel', 'Microsoft Office'],
      "Microsoft Silverlight Training Classes" => ['app development'],
      "Microsoft Sketchflow Training Classes" => [
        'UX design',
        'user experience',
        'prototyping',
      ],
      "Muse Certification Training" => [
        'Adobe',
        'Creative Cloud',
        'web design',
      ],
      "OS X Migration Services" => ['Apple'],
      "Photography Classes" => ['digital imaging', 'photography'],
      "Photoshop Certification Training" => [
        'digital imaging',
        'graphic design',
      ],
      "Photoshop Classes" => [
        'Adobe',
        'Creative Cloud',
        'digital imaging',
        'graphic design',
      ],
      "Photoshop Classes for Web Graphics Training" => [
        'Creative Cloud',
        'digital imaging',
      ],
      "Photoshop for Beginners Classes" => [
        'Creative Cloud',
        'digital imaging',
      ],
      "PHP Training Classes" => ['web development'],
      "PowerPoint Design Training Classes" => [
        'slide design',
        'presentation design',
      ],
      "PowerPoint Training Classes" => ['slide design'],
      "Premiere Pro Certification Training" => [
        'Creative Cloud',
        'video editing',
        'animation',
      ],
      "Presentation Design Training Classes" => [
        'visual design',
        'presentation design',
      ],
      "Print Design Training Classes" => [
        'Creative Cloud',
        'graphic design',
        'page layout',
      ],
      "Printing Training Classes" => [
        'publishing',
        'digital imaging',
        'digital publishing',
      ],
      "Python Training Classes" => [
        'web coding',
        'web development',
        'data analysis',
      ],
      "QuarkXPress Training Classes" => ['digital publishing', 'page layout'],
      "Responsive Web Design Classes" => ['HTML & CSS', 'web design'],
      "Section 508 Compliance Training Classes" => [
        'Section 508',
        'web accessibility',
      ],
      "Sketch Training" => ['visual design', 'UX design', 'prototyping'],
      "SketchUp Classes" => ['visual design', 'UX design', 'prototyping'],
      "Technical Communication Suite Training" => [
        'e-learning',
        'online learning',
      ],
      "Typography Courses & Classes" => ['graphic design'],
      "UX Certification" => ['user experience'],
      "UX Classes and UX Courses" => [
        'UX design',
        'user experience',
        'prototyping',
      ],
      "Video Editing Bootcamp" => ['video editing', 'motion graphics'],
      "Video Editing Classes" => [
        'video editing',
        'animation',
        'special effects',
      ],
      "Visio Training Classes" => ['data visualization', 'data analysis'],
      "Web Accessibility Training Classes" => ['Section 508', 'accessibility'],
      "Web Design Classes" => [
        'UX design',
        'HTML & CSS',
        'web design',
        'web development',
      ],
      "Web Development Training Classes" => [
        'web coding',
        'HTML & CSS',
        'web development',
      ],
      "Windows App Design Training Classes" => ['UX design'],
      "WordPress Classes" => ['HTML & CSS', 'web design'],
      "Workforce Training Fund Courses" => [
        'Creative Cloud',
        'e-learning',
        'online learning',
        'video editing',
        'motion graphics',
      ],
      "XML Training Classes" => [
        'publishing',
        'digital publishing',
        'page layout',
      ],
    ];

    $industries_served = $category_industries[$value];
    if (is_array($industries_served)) {
      $names = [];
      foreach ($industries_served as $name) {
        $names[] = $name;
      }
      return $names;
    }
    return NULL;
  }

}

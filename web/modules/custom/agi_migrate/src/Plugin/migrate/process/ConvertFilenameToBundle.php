<?php

namespace Drupal\agi_migrate\Plugin\migrate\process;

use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\Row;

/**
 * Provides a 'ConvertFilenameToBundle' migrate process plugin.
 *
 * @MigrateProcessPlugin(
 *  id = "convert_filename_to_bundle"
 * )
 */
class ConvertFilenameToBundle extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $type = explode('.', $value);
    if (is_array($type)) {
      switch (strtolower(end($type))) {
        case 'png':
        case 'gif':
        case 'jpg':
        case 'jpeg':
        case 'svg':
          return 'image';

        case 'pdf':
        case 'csv':
        case 'epub':
        case 'zip':
          return 'document';

        case 'mp4':
        case 'm4v':
          return 'video';

      }
    }
  }

}

<?php

namespace Drupal\agi_migrate\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * The 'xmlsitemap_custom' source plugin.
 *
 * @MigrateSource(
 *   id = "xmlsitemap_custom",
 *   source_module = "agi_migrate"
 * )
 */
class XmlsitemapCustom extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('xmlsitemap', 'xsm')
      ->condition('type', 'custom')
      ->fields('xsm', ['id', 'type', 'loc', 'access', 'status', 'status_override', 'lastmod', 'language', 'priority', 'priority_override', 'changefreq' ]);
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'id' => $this->t('The record ID.'),
      'type' => $this->t('Primary key with id. the type of item'),
      'loc' => $this->t('The URL to the item relative to the Drupal path'),
      'access' => $this->t('A boolean the represents if the item is viewable by the anonymous user'),
      'status' => $this->t('An integer that represents if the item is included in the sitemap'),
      'status_override' => $this->t('A boolean that if TRUE means that the status field has been overrideen from its default value'),
      'lastmod' => $this->t('The UNIX timestamp of last modification of the item'),
      'language' => $this->t('The language'),
      'priority' => $this->t('The priority of this URL relataive to other URLS on your site. Valid values range from 0.0 to 1.0'),
      'priority_override' => $this->t('A boolean that if TRUE means that the priority field has been overridden from its default value'),
      'changefreq' => $this->t('The average time in seconds between changes of this item'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['id']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {

    // @DCG
    // Extend/modify the row here if needed.
    //
    // Example:
    // @code
    // $name = $row->getSourceProperty('name');
    // $row->setSourceProperty('name', Html::escape('$name');
    // @endcode
    return parent::prepareRow($row);
  }

}

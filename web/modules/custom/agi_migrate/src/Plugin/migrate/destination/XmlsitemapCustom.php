<?php

namespace Drupal\agi_migrate\Plugin\migrate\destination;

use Drupal\Core\Database\Connection;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\Plugin\migrate\destination\DestinationBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The 'xmlsitemap_custom_destination' destination plugin.
 *
 * @MigrateDestination(
 *   id = "xmlsitemap_custom"
 * )
 */
class XmlsitemapCustom extends DestinationBase implements ContainerFactoryPluginInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs the plugin.
   *
   * @param array $configuration
   *   Plugin configuration.
   * @param string $plugin_id
   *   The plugin ID.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\migrate\Plugin\MigrationInterface $migration
   *   The current migration.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration, Connection $connection) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $migration);
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, MigrationInterface $migration = NULL) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $migration,
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function import(Row $row, array $old_destination_id_values = []) {

    // Save the row to custom table.
    $this->connection
      ->insert('xmlsitemap')
      ->fields([
        'id' => $row->getDestinationProperty('id'),
        'type' => $row->getDestinationProperty('type'),
        'loc' => $row->getDestinationProperty('loc'),
        'status' => $row->getDestinationProperty('status'),
        'language' => $row->getDestinationProperty('language'),
        'access' => $row->getDestinationProperty('access'),
        'status_override' => $row->getDestinationProperty('status_override'),
        'lastmod' => $row->getDestinationProperty('lastmod'),
        'priority' => $row->getDestinationProperty('priority'),
        'priority_override' => $row->getDestinationProperty('priority_override'),
      ])
      ->execute();

    return [$row->getDestinationProperty('id')];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    $ids['id']['type'] = 'integer';
    return $ids;
  }

  /**
   * {@inheritdoc}
   */
  public function fields(MigrationInterface $migration = NULL) {
    return [
      'id' => $this->t('The record ID.'),
      'type' => $this->t('Primary key with id. the type of item'),
      'loc' => $this->t('The URL to the item relative to the Drupal path'),
      'access' => $this->t('A boolean the represents if the item is viewable by the anonymous user'),
      'status' => $this->t('An integer that represents if the item is included in the sitemap'),
      'status_override' => $this->t('A boolean that if TRUE means that the status field has been overridden from its default value'),
      'lastmod' => $this->t('The UNIX timestamp of last modification of the item'),
      'priority' => $this->t('The priority of this URL relataive to other URLS on your site. Valid values range from 0.0 to 1.0'),
      'priority_override' => $this->t('A boolean that if TRUE means that the priority field has been overridden from its default value'),
    ];
  }

}

<?php

namespace Drupal\agi_near_me\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\Core\Controller\ControllerBase;

class AgiNearMe extends ControllerBase {
  protected function getCategoryNode($category) {
    // Checking if the category is valid.
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'subject')
      ->condition('field_short_category_name', str_replace('-', ' ', $category), '=');

    $results = $query->execute();
    $category_node = end($results);

    return $category_node;
  }

  public function allPage($category) {
    $category_node = $this->getCategoryNode($category);

    if ($category_node) {
      return [
        '#theme' => 'agi_near_me_page',
      ];
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  public function getTitle($category) {
    return ucwords(str_replace('-', ' ', $category)) . ' Classes Near Me | ' . \Drupal::config('system.site')->get('name');
  }
}

<?php

namespace Drupal\agi_tokens;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Render\Markup;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\file\Entity\File;

/**
 * Class that define the service to fetch custom info before creating the tokens.
 */
class AgiTokensService {

  /**
   * Getting the current Node Title.
   */
  public function getCurrentNodeTitle($current_node) {
    if ($current_node instanceof NodeInterface) {
      // Getting the title.
      return $current_node->label();
    }

    return '';
  }

  /**
   * Getting the page Title based on the URL.
   */
  public function getPageTitleFromURL() {
    $current_path = \Drupal::service('path.current')->getPath();

    // Get the last value of the path.
    $exploded_path = explode('/', $current_path);
    $path_alias_position = end($exploded_path);

    return ucwords(str_replace('-', ' ', $path_alias_position));
  }

  /**
   * Getting the Category Short Name.
   */
  public function getCategoryShortName($current_node) {
    if ($current_node instanceof NodeInterface) {
      // Loading the referenced node.
      switch ($current_node->bundle()) {
        case 'category_location_page':
        case 'seconday_location_category_page':
          $reference_node = Node::load($current_node->field_cat_for_location->target_id);
          $short_name = $reference_node->field_short_category_name->value;
          break;

        case 'subject':
          $short_name = $current_node->field_short_category_name->value;
          break;

        case 'course':
          $primary_category = $current_node->get('field_primary_category')->getValue();
          $category_nid = NULL;

          if (!empty($primary_category)) {
            $category_nid = $current_node->get('field_primary_category')->first()->getValue();
          }

          if ($category_nid && isset($category_nid['target_id'])) {
            $reference_node = Node::load($category_nid['target_id']);
            $short_name = $reference_node->field_short_category_name->value;
          }

          break;
      }
    }

    // Returning the short name field from the loaded node.
    return isset($short_name) && !empty($short_name) ? $short_name : '';
  }

  /**
   * Getting the Category Title.
   */
  public function getCategoryTitle($current_node) {
    if ($current_node instanceof NodeInterface) {
      // Loading the referenced node.
      switch ($current_node->bundle()) {
        case 'category_location_page':
        case 'seconday_location_category_page':
          $reference_node = Node::load($current_node->field_cat_for_location->target_id);
          $category_title = $reference_node->label();
          break;

        case 'subject':
          $category_title = $current_node->label();
          break;

        case 'course':
          $primary_category = $current_node->get('field_primary_category')->getValue();
          $category_nid = NULL;

          if (!empty($primary_category)) {
            $category_nid = $current_node->get('field_primary_category')->first()->getValue();
          }

          if ($category_nid && isset($category_nid['target_id'])) {
            $reference_node = Node::load($category_nid['target_id']);
            $category_title = $reference_node->label();
          }

          break;
      }
    }

    // Returning the title from the loaded node.
    return isset($category_title) && !empty($category_title) ? $category_title : '';
  }

  /**
   * Getting the Category Short Name.
   */
  public function getCategoryFullName($current_node) {
    if ($current_node instanceof NodeInterface) {
      // Loading the referenced node.
      switch ($current_node->bundle()) {
        case 'category_location_page':
        case 'seconday_location_category_page':
          $reference_node = Node::load($current_node->field_cat_for_location->target_id);
          $full_name = $reference_node->field_subject_name->value;
          break;

        case 'subject':
          $full_name = $current_node->field_subject_name->value;
          break;

        case 'course':
          $primary_category = $current_node->get('field_primary_category')->getValue();
          $category_nid = NULL;

          if (!empty($primary_category)) {
            $category_nid = $current_node->get('field_primary_category')->first()->getValue();
          }

          if ($category_nid && isset($category_nid['target_id'])) {
            $reference_node = Node::load($category_nid['target_id']);
            $full_name = $reference_node->field_subject_name->value;
          }

          break;
      }
    }

    // Returning the shor name field from the loaded node.
    return isset($full_name) && !empty($full_name) ? $full_name : '';
  }

  /**
   * Getting the Location City Short Name.
   */
  public function getLocationCityShortName($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'category_location_page':
          $reference_node = Node::load($current_node->field_location_for_cat->target_id);
          $city_name = $reference_node->field_city_for_reference_pages->value;
          break;
        case 'location':
          $city_name = $current_node->field_city_for_reference_pages->value;
          break;
        case 'seconday_location_category_page':
          $city_name = NULL;
          $secondary_location = Node::load($current_node->field_secondary_location->target_id);
          $city_name = $secondary_location->label();
      }
    }

    return isset($city_name) && !empty($city_name) ? $city_name : '';
  }

  /**
   * Getting the Location City Short Name with a connector.
   */
  public function getLocationCityShortNameConnector($current_node) {
    $city_name = $this->getLocationCityShortName($current_node);

    if ($city_name !== 'Online') {
      return t('in ') . $city_name;
    }

    return $city_name;
  }

  /**
   * Getting the Near Location City Short Name.
   */
  public function getLocationNearCityShortName($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_category_page':
          $city_name = NULL;
          $secondary_location = Node::load($current_node->field_secondary_location->target_id);

          if (!$secondary_location->get('field_nearby_primary_location')->isEmpty()) {
            $primary_location = Node::load($secondary_location->field_nearby_primary_location->target_id);
            $city_name = $primary_location->field_city_for_reference_pages->value;
          }
      }
    }

    return isset($city_name) && !empty($city_name) ? $city_name : '';
  }

  /**
   * Getting the Location City Short Name.
   */
  public function getLocationClients($current_node) {
    if ($current_node instanceof NodeInterface) {
      // Loading the referenced node.
      $reference_node = Node::load($current_node->field_location_for_cat->target_id);

      // Returning the string of location clients field from the loaded node.
      $clients = $reference_node->field_location_clients->value;
    }

    return isset($clients) && !empty($clients) ? $clients : '';
  }


  /**
   * Getting the Location City Short Name.
   */
  public function getLocationStateName($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_category_page':
          $state_name = NULL;
          $secondary_location = Node::load($current_node->field_secondary_location->target_id);

          if (!$secondary_location->get('field_nearby_primary_location')->isEmpty()) {
            $primary_location = Node::load($secondary_location->field_nearby_primary_location->target_id);
            $state_name = $primary_location->field_location_state->value;
          }
          break;
        default:
          $reference_node = Node::load($current_node->field_location_for_cat->target_id);
          $state_name = $reference_node->field_location_state->value;
          break;
      }
    }

    return isset($state_name) && !empty($state_name) ? $state_name : '';
  }

  /**
   * Getting the Category Industries Served.
   */
  public function getCategoryIndustriesServed($current_node) {
    if ($current_node instanceof NodeInterface && $current_node->bundle() == 'category_location_page') {
      $reference_node = Node::load($current_node->field_cat_for_location->target_id);
      if(!$reference_node->get('field_industries_served')->isEmpty()) {
        $industries_served_terms = $reference_node->get('field_industries_served')
          ->referencedEntities();
        foreach ($industries_served_terms as $term) {
          $terms[] = $term->getName();
        }
        if(is_array($terms)) {
          return join(' and ', array_filter(array_merge(array(join(', ', array_slice($terms, 0, -1))), array_slice($terms, -1)), 'strlen'));
        }
      }
    }

    // Returning the shor name field from the loaded node.
    return isset($industries_served) && !empty($industries_served) ? $industries_served : '';
  }

  /**
   * Getting the Category Industries Served.
   */
  public function getCategorySkills($current_node) {
    if ($current_node instanceof NodeInterface && $current_node->bundle() == 'category_location_page') {
      $reference_node = Node::load($current_node->field_cat_for_location->target_id);
      if(!$reference_node->get('field_category_skill')->isEmpty()) {
        $category_skills = $reference_node->get('field_category_skill')
          ->referencedEntities();
        foreach ($category_skills as $term) {
          $terms[] = $term->getName();
        }
        if(is_array($terms)) {
          return implode(', ', $terms);
        }
      }
    }

    // Returning the shor name field from the loaded node.
    return isset($industries_served) && !empty($industries_served) ? $industries_served : '';
  }

  /**
   * Getting the Location City Short Name.
   */
  public function getLocationAdditionalInfo($current_node) {
    if ($current_node instanceof NodeInterface && $current_node->bundle() === 'category_location_page') {
      // Loading the referenced node.
      $reference_node = Node::load($current_node->field_location_for_cat->target_id);

      // Return the additional information text for the Location.
      $additional_info_value = $reference_node->get('field_additional_information')->getValue();
      $additional_info = !empty($additional_info_value) ? $additional_info_value[0]['value'] : '';
      $token = \Drupal::token();
      $additional_info = strip_tags($token->replace($additional_info, ['node' => $current_node]));
    }

    return isset($additional_info) && !empty($additional_info) ? $additional_info : '';
  }

  /**
   * Getting the Location City Short Name.
   */
  public function getCategoryAdditionalInfo($current_node) {
    if ($current_node instanceof NodeInterface && $current_node->bundle() === 'category_location_page') {
      // Loading the referenced node.
      $reference_node = Node::load($current_node->field_cat_for_location->target_id);

      // Return the additional information text for the Category.
      $first_info = $reference_node->get('field_category_additional_info')->first();

      if ($reference_node && $first_info) {
        $additional_info = $reference_node->get('field_category_additional_info')->first()->getValue()['value'];
        $token = \Drupal::token();
        $additional_info = strip_tags($token->replace($additional_info, ['node' => $current_node]));
      }
    }

    return isset($additional_info) && !empty($additional_info) ? $additional_info : '';
  }

  /**
   * Getting the Category Subject.
   */
  public function getCategorySubjects($current_node) {
    if ($current_node instanceof NodeInterface) {
      // @TODO: Fix this.
      $subjects = 'Digital design';
    }

    return isset($subjects) && !empty($subjects) ? $subjects : '';
  }

  /**
   * Getting the Location City Short Name.
   */
  public function getLocationImages($current_node, $index) {
    if ($current_node instanceof NodeInterface && $current_node->hasField('field_location_for_cat')) {
      // Load the referenced node.
      $reference_node = Node::load($current_node->field_location_for_cat->target_id);
      // Fetch the media entity from the field based on array index.
      $location_image_media = !$reference_node->get('field_primary_image_slides_media')->isEmpty() ? $reference_node->get('field_primary_image_slides_media')->referencedEntities()[$index]->id() : NULL;
      if (isset($location_image_media)) {
        return $location_image_media;
      }
    }
  }

  /**
   * Getting the Location City Short Name.
   */
  public function getCategoryLogo($current_node) {
    if ($current_node instanceof NodeInterface && $current_node->hasField('field_right_header_image')) {
      $fid = $current_node->field_left_header_image->target_id;
      if ($fid) {
        $file = File::load($fid);
      }
      if ($file) {
        $variables = [
          'responsive_image_style_id' => 'category_logo',
          'uri' => $file->getFileUri(),
        ];
      }
      $image = \Drupal::service('image.factory')->get($file->getFileUri());
      if ($image->isValid()) {
        $variables['width'] = $image->getWidth();
        $variables['height'] = $image->getHeight();
      }
      else {
        $variables['width'] = $variables['height'] = NULL;
      }
      $logo_build = [
        '#theme' => 'responsive_image',
        '#width' => $variables['width'],
        '#height' => $variables['height'],
        '#responsive_image_style_id' => $variables['responsive_image_style_id'],
        '#uri' => $variables['uri'],
      ];

      $renderer = \Drupal::service('renderer');
      $renderer->addCacheableDependency($logo_build, $file);

      // Returning the shor name field from the loaded node.
      $responsive_image = $renderer->renderRoot($logo_build);
      return $responsive_image;
    }
    else {
      return '';
    }
  }

  /**
   * Getting the Location City Short Name.
   */
  public function getCategoryProduct($current_node) {
    if ($current_node instanceof NodeInterface && $current_node->hasField('field_right_header_image')) {
      return 'Product';
    }
  }

  /**
   * Getting the average reviews given the NID.
   */
  public function getAverageReviews($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'category_location_page':
          $cat_for_location = $current_node->field_cat_for_location->target_id;
          break;
      }
    }

    $reviews_nids = \Drupal::entityQuery('node')
      ->condition('type', 'reviews')
      ->condition('status', 1)
      ->condition('body', '', '<>')
      ->condition('field_review_category', $cat_for_location)
      ->sort('created', 'DESC')
      ->execute();

    $reviews = Node::loadMultiple($reviews_nids);
    $sum_rating = 0;

    foreach ($reviews as $review) {
      $sum_rating += $review->field_review_rating->value;
    }

    return number_format($sum_rating / count($reviews_nids), 1);
  }

  /**
   * Getting the short address for a location.
   */
  public function getLocationShortAddress($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_page':
          return $current_node->field_address_city->value . ', ' . $current_node->field_state_abbreviated_->value;
          break;
        case 'seconday_location_category_page':
          $reference_node = Node::load($current_node->field_secondary_location->target_id);
          return $reference_node->field_address_city->value . ', ' . $reference_node->field_state_abbreviated_->value;
      }
    }
  }

  /**
   * Getting the city for a secondary location.
   */
  public function getSecondaryLocationCityName($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_page':
          return $current_node->field_address_city->value;
          break;
        case 'seconday_location_category_page':
          $reference_node = Node::load($current_node->field_secondary_location->target_id);
          return $reference_node->field_address_city->value;
      }
    }
  }

  /**
   * Getting the state for a secondary location.
   */
  public function getSecondaryLocationStateName($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_page':
          return $current_node->field_address_state->value;
          break;
        case 'seconday_location_category_page':
          $reference_node = Node::load($current_node->field_secondary_location->target_id);
          return $reference_node->field_address_state->value;
      }
    }
  }

  /**
   * Getting the url for the primary city of a secondary location page.
   */
  public function getLocationPrimaryCiryUrl($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_page':
          // Load the city.
          if (isset($current_node->field_nearby_primary_location) && isset($current_node->field_nearby_primary_location->target_id)) {
            $reference_node = Node::load($current_node->field_nearby_primary_location->target_id);

            return Link::fromTextAndUrl($reference_node->field_city_for_reference_pages->value, Url::fromUri('internal:/node/' . $reference_node->id()))->toString();
          }

          return '';

          break;
      }
    }
  }

  /**
   * Getting the category for a specific location.
   */
  public function getCategoryUrlForLocation($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_category_page':
          // Getting the reference.
          $secondary_location = Node::load($current_node->field_secondary_location->target_id);
          $nearby_primary_location = $secondary_location->field_nearby_primary_location->target_id;

          $category_location_id = \Drupal::entityQuery('node')
            ->condition('type', 'category_location_page')
            ->condition('field_cat_for_location', $current_node->field_cat_for_location->target_id)
            ->condition('field_location_for_cat', $nearby_primary_location)
            ->execute();

          $category_location_id = implode('', $category_location_id);

          return \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $category_location_id);
          break;
      }
    }
  }

  /**
   * Getting the certificate program per category.
   */
  public function getCategoryRelatedCertificateProgram($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_category_page':
          // Getting the reference.
          $certificate_programs = $current_node->field_cat_for_location->entity->get('field_certificate_programs')->referencedEntities();

          if (is_array($certificate_programs)) {
            foreach ($certificate_programs as $certificate_program) {
              $related_certificate_programs[] = Link::fromTextAndUrl($certificate_program->label(), Url::fromUri('internal:/node/' . $certificate_program->id()))->toString();
            }

            return Markup::create(implode(', ', $related_certificate_programs));
          }

          return '';
          break;
      }
    }
  }

  /**
   * Getting the location URL.
   */
  public function getLocationUrl($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_category_page':
          // Getting the reference.
          $secondary_location_nid = $current_node->field_secondary_location->target_id;

          return \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $secondary_location_nid);
          break;
      }
    }
  }

  /**
   * Getting the location Certification Info.
   */
  public function getLocationCertificationInfo($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_category_page':
          // Getting the reference.
          $category = Node::load($current_node->field_cat_for_location->target_id);

          if (!$category) {
            return '';
          }

          if (isset($category->field_certificate_programs->target_id)) {
            $certificate_info = Node::load($category->field_certificate_programs->target_id);

            return Link::fromTextAndUrl($certificate_info->label(), Url::fromUri('internal:/node/' . $certificate_info->id()))->toString();
          }

          return '';
          break;
      }
    }
  }

  /**
   * Getting the events heading info.
   */
  public function getEventsHeadingInfo($current_node) {
    if ($current_node instanceof NodeInterface) {
      return $current_node->label();
    }

    return '';
  }

  /**
   * Getting the events heading info.
   */
  public function getCategoryByUrl() {
    $current_path = explode('/', \Drupal::service('path.current')->getPath());
    $category = isset($current_path[2]) ? ucwords(str_replace('-', ' ', $current_path[2])) : '';

    return $category . ' ' . t('Classes');
  }

  /**
   * Fetching the near me section URL.
   */
  public function getNearMeUrl($current_node) {
    $near_me_url = '';

    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'seconday_location_category_page':
          $short_category_name = str_replace(' ', '-', strtolower($current_node->field_cat_for_location->entity->field_short_category_name->value));
          $current_path = \Drupal::service('path.current')->getPath();
          $current_path_alias = \Drupal::service('path_alias.manager')->getAliasByPath($current_path);
          $current_path_fragments = explode('/', $current_path_alias);
          $first_path = ($current_path_fragments[1] !== 'adobe' && $current_path_fragments[1] !== 'apple') ? 'all' : $current_path_fragments[1];

          // Creating the URL based off of the Short Category Name.
          $near_me_url = '/' . $first_path . '/' . $short_category_name . '/classes/near-me';
          break;
      }
    }

    return $near_me_url;
  }

  /**
   * Returns the field value given a field name.
   */
  public function getFieldValues($current_node, $token_name) {
    $field_name = 'field_' . $token_name;

    if ($current_node instanceof NodeInterface && $current_node->hasField($field_name)) {
      $field_value = $current_node->get($field_name)->getValue();

      if (!empty($field_value)) {
        return $current_node->get($field_name)->getValue()[0]['value'];
      }
    }

    return '';
  }

  /**
   * Returns the name of the Certificate Program.
   */
  public function getCertificateProgramName($current_node) {
    if ($current_node instanceof NodeInterface) {
      switch ($current_node->bundle()) {
        case 'certificate_program':
          return $field_value = 'CP-' . str_replace(' ', '-', $current_node->field_cp_short_name->value);
          break;
      }
    }

    return '';
  }

}
